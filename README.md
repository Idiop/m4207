Journal de bord du Lundi 30 Janvier:

Aujourd'hui, après une journée longue et quelque peu exténuante, nous avons terminé les cours par deux heures de programmation sous Arduino. Je me suis rendu compte qu'à la séance 
précédente je n'avais pas mis sur le git les travaux que j'avais fais dans le TD1, en l'occurrence, faire fonctionner la led ainsi que bien avant cela le débug des ports séries
la réinstallation de Arduino puisque ma version était obsolète, et la réinstallation de tous les composants liés à la carte Linkit justement.
J'ai donc pour le premier exercice fait clignoté ma led avec ce code:

```c
void setup() {
  // put your setup code here, to run once:
  pinMode(13,OUTPUT);  
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  Serial.print("Hello World");
  delay(5000);
  digitalWrite(13,LOW);
  delay(5000);
}
```

Pour renvoyer un message dans le port série nous avons donc utilisé le code suivant:

```c
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("Hello World");
  
}
```

On affiche une incrémentation de 1 d'une variable x:
```c
int x = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  x = x+1;
  Serial.print("the value of the counter is ");
  Serial.println(x);
  delay(1000);
}
```

Pour ce qui est de l'exercice sur le GPS, j'ai d'abord cherché sur la doc comment brancher quel module à quel port de la carte pour faire du GPS, ensuite j'ai utilisé le code
du GPS présent dans les exemples Arduino:

```c
#include <LGPS.h>

gpsSentenceInfoStruct info;
char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(2000);
}
```
Séance 4: DIOP Ismaël 8/02/2017

Lendemain de tempête ou cyclone ou dépression tropicale, je ne sais pas vraiment.
Aujourd’hui le temps est plutôt lourd et malgrés l’absence de soleil, la chaleur est elle, bien présente.
Bon pour cette séance, je n’ai malheureusement pas pu faire énormément de choses, j’ai d’abord eu des difficultés à relancer l’écran LCD comme je l’avais codé à MA séance 
précédente, j’ai finalement pu. Ensuite j’ai tenté (en vain) de scanner les réseaux alentours à l’aide des codes d’exemples et en fouillant un peu sur Internet. Tâche 
extrêmement ardue je trouve puisque je n’ai abouti à rien à ce sujet là. (Il faudrait pouvoir trouver un moyen de la localiser autre que le GPS et avec ou sans la WiFi, 
je laisse vos suggestions éclairer le chemin à aborder…).
Néanmoins j’ai effectué des tests de GPS avec ce code ci: 

```c

#include <LGPS.h>

gpsSentenceInfoStruct info;
char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(2000);
}

```c

Ensuite j’ai effectué des tests avec la batterie en mettant la valeur, le seuil sous lequel l’écran changera de couleur et d’affichage à 100. Et j’ai rassemblé le code de la 
Led et de l’écran LCD sous le nom de projet. Je vous laisse regarder tout ça:

```c

#include <Wire.h>
#include "rgb_lcd.h"
#include <LBattery.h>
#define LED 2 //connect LED to digital pin2

char buff[256];
rgb_lcd lcd;

int colorR = 0;
int colorG = 255;
int colorB = 0;


void setup() {                
  // initialize the digital pin2 as an output.
  pinMode(LED, OUTPUT);     
    lcd.begin(16, 2);
    
    lcd.setRGB(colorR, colorG, colorB);
    
    // Print a message to the LCD.
    lcd.print("Bonjour !");

    delay(1000);
}
 
void loop() {
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  if ( LBattery.level() > 20){
  digitalWrite(LED, HIGH);   // set the LED on
  delay(1000);               // for 500ms
  digitalWrite(LED, LOW);   // set the LED off
  delay(1000);
  }
  else if ( LBattery.level()<20) {
    digitalWrite(LED, HIGH);  
    delay(250);
  }
  delay(1000);

     lcd.setCursor(0, 1);
    // print the number of seconds since reset:

    if ( LBattery.level() >= 100){
      lcd.print("Batterie: OK");
      delay(1000);    
    }
    else if ( LBattery.level()<=100) {
      lcd.setRGB(255,0,0);
      lcd.print("[!] Batterie < 20% [!]");
      delay(1000);
    }

    delay(1000);
}
```

Quelques suggestions pour la suite:

Réussir à effectuer des tests avec la batterie concluants sur tout l’ensemble du programme projet.
Trouver un moyen de repérer le dispositif avec la Wifi, ne serait ce que de pouvoir afficher les coordonnées GPS et les recevoir sur l’ordinateur en utilisant la Wifi.
Pour ce TP là je n’ai pas vraiment utilisé de sites Internet donc je n’ai aucune sources à vous soumettre. Fin de la transmission.


J’ai la flemme d’écrire, mais en gros:
J’ai repris un peu vos codes, j’ai essayé de comprendre vu que c’est pas expliqué comment, j’ai modifié quelques trucs pour rendre ça plus utilitaires hein Warren. 
Et j’ai plutôt imaginé ce qu’il fallait faire pour la suite parce que ce qu’il nous reste à entreprendre est de tracer la position de la clé ainsi en continuité de vos superbes 
idées j’me suis dis. On va se connecter à deux réseaux pour déterminer avec plus ou moins de précision la position de la clé par rapport aux signaux, on l'accole à une 
cartographie de l’IUT et bingo. Tout ça par WiFi. Y a l’idée de l’faire par la position GPS, mais faudrait se renseigner avec Stéphane ou Roman qui s’y connaissent très bien à 
ce niveau là et arrive à déterminer avec précision la position de leur clé. 

```c

#include <SPI.h>
#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiClient.h>


#define WIFI_AP "iPhone de Ismaël"
#define WIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
void setup() {
  //Initialize serial and wait for port to open:
  LTask.begin();
  LWiFi.begin();
  
  Serial.println("Connecting to AP");
  Serial.begin(9600);
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_OPEN)))
    {
      delay(1000);
    }
  
}

void loop() {
  // scan for existing networks:
  Serial.println("Scanning available networks...");
  listNetworks();
  delay(10000);

 Serial.println("Connected!");
    while(1){
      LWifiStatus ws = LWiFi.status();
      boolean status = wifi_status(ws);
      if(!status){
        Serial.println("Connecting to AP");
        while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_OPEN)))
        {
          delay(500);
        }
      }
    }
}


  // I plan to use this function in the future to reconnect to WiFi if the connection drops:
  boolean wifi_status(LWifiStatus ws){
    switch(ws){
      case LWIFI_STATUS_DISABLED:
        return false;
      break;
      case LWIFI_STATUS_DISCONNECTED:
        return false;
      break;
      case LWIFI_STATUS_CONNECTED:
        return true;
      break;
    }
    return false;
  }
  
void listNetworks() {

  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = LWiFi.scanNetworks();
  if (numSsid == -1) {
        Serial.println("Couldn't get a wifi connection");
        while (true);
  }

  // print the list of networks seen:
  Serial.print("number of available networks:");
  Serial.println(numSsid);
  
  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
        Serial.print(thisNet);
        Serial.print(") ");
        Serial.print(LWiFi.SSID(thisNet));
        Serial.print("\tSignal: ");
        Serial.print(LWiFi.RSSI(thisNet));
        Serial.print(" dBm");

    // Adresse MAC du routeur attaché:
        byte bssid[6];
        LWiFi.BSSID(bssid);    
        Serial.print(" SSID : ");
        Serial.print(bssid[0],HEX);
        Serial.print(":");
        Serial.print(bssid[1],HEX);
        Serial.print(":");
        Serial.print(bssid[2],HEX);
        Serial.print(":");
        Serial.print(bssid[3],HEX);
        Serial.print(":");
        Serial.print(bssid[4],HEX);
        Serial.print(":");
        Serial.println(bssid[5],HEX);
  }

  
}

```

Dans cette séance j’ai ouvert deux issues sur le GIT et téléchargé le Git sur mon PC. J’ai essayé de réfléchir sur le système de traçage très précis là dont j’ai parlé une 
séance plus tôt mais c’est pas facile du tout. Néanmoins j’arrive à afficher les routeurs autour, leurs adresses MAC, à me connecter à un d’entre eux:

```c

#include <SPI.h>
#include <LTask.h>
#include <LWiFi.h>
#include <LWiFiClient.h>
  #define WIFI_AP "RT-WIFI-Guest"
  #define WIFI_PASSWORD "wifirtguest"
  #define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
void setup() {
  //Initialize serial and wait for port to open:
  LTask.begin();
  LWiFi.begin();
  
  Serial.println("Connecting to AP");
  Serial.begin(9600);
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
    {
     
      delay(1000);
    }
  
 //printMacAddress();
}
void loop() {
  // scan for existing networks:
  Serial.println("Scanning available networks...");
  listNetworks();
  delay(10000);
 Serial.println("Connected!");
    while(1){
      LWifiStatus ws = LWiFi.status();
      boolean status = wifi_status(ws);
      if(!status){
        Serial.println("Connecting to AP");
        while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
        {
          delay(500);
        }
      }
    }
    delay(500);
}
  // I plan to use this function in the future to reconnect to WiFi if the connection drops:
  boolean wifi_status(LWifiStatus ws){
    switch(ws){
      case LWIFI_STATUS_DISABLED:
        return false;
      break;
      case LWIFI_STATUS_DISCONNECTED:
        return false;
      break;
      case LWIFI_STATUS_CONNECTED:
        return true;
      break;
    }
    return false;
  }
void printMacAddress() {
  // the MAC address of your Wifi shield
  byte mac[6];
  int i;
  // print your MAC address:
  LWiFi.macAddress(mac);
  Serial.print("MAC: ");
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);
  for(i=0;i<sizeof(mac);i++){
    mac[i]=0;
  }
}
void listNetworks() {
  // scan for nearby networks:
 byte mac[6];
  int i;
  Serial.println("** Scan Networks **");
  int numSsid = LWiFi.scanNetworks();
  if (numSsid == -1) {
    Serial.println("Couldn't get a wifi connection");
    while (true);
  }
  // print the list of networks seen:
  Serial.print("number of available networks:");
  Serial.println(numSsid);
  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    Serial.print(thisNet);
    Serial.print(") ");
    Serial.print(LWiFi.SSID(thisNet));
    Serial.print("\tSignal: ");
    Serial.print(LWiFi.RSSI(thisNet));
    Serial.print(" dBm");
  //trying to recode printMacAddress
  LWiFi.macAddress(mac);
  Serial.print("MAC: ");
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);
  for(i=0;i<sizeof(mac);i++){
    mac[i]=0;
  }
  
    
    //MAC address of routers
    byte bssid[6];
    LWiFi.BSSID(bssid);    
    Serial.print(" SSID : ");
    Serial.print(bssid[0],HEX);
    Serial.print(":");
    Serial.print(bssid[1],HEX);
    Serial.print(":");
    Serial.print(bssid[2],HEX);
    Serial.print(":");
    Serial.print(bssid[3],HEX);
    Serial.print(":");
    Serial.print(bssid[4],HEX);
    Serial.print(":");
    Serial.println(bssid[5],HEX);
  }
  
}

```


Début de séance:

Je pense créer un système qui me permettrait d’enregistrer ne serait ce que l’utilisateur de la clé.

C’est la merde sérieux. 

Euh honnêtement j’ai trouvé quelques solutions ultra compliquées à mettre en place, mais franchement on va devoir bosser ça en dehors des heures de TP c’est tout simplement 
trop hardcore.

Ma première solution proposée est une idée de Dylan qui consiste à l’aide de python d’utiliser un socket pour envoyer en continu des données sur un serveur web qui va les 
stocker sur une BDD. Ensuite afficher ces données sur une page web. 

La deuxième consite à utiliser un QR code et un serveur web localhost genre WAMPP ou XAMPP mais honnêtement c’est pas mieux. 

J'aimerais bien qu'ils travaillent dessus pour la prochaine séance et même en dehors, je vais aussi m'y consacrer, mais c'est primordial.